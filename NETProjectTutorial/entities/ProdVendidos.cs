﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ProdVendidos
    {
       private int cod;
       private string producto;
       private int descuento;
       private int cantidad;
       private double precio;
       private double total;

        public int Cod
        {
            get
            {
                return cod;
            }

            set
            {
                cod = value;
            }
        }

        public string Producto
        {
            get
            {
                return producto;
            }

            set
            {
                producto = value;
            }
        }

        public int Descuento
        {
            get
            {
                return descuento;
            }

            set
            {
                descuento = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public ProdVendidos(int cod, string producto, int descuento, int cantidad, double precio, double total)
        {
            this.Cod = cod;
            this.Producto = producto;
            this.Descuento = descuento;
            this.Cantidad = cantidad;
            this.Precio = precio;
            this.Total = total;
        }
    }
}
